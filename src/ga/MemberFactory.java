package ga;

/**
 * interface tovarny na jedince
 * 
 * @author Jiří Lýsek
 */
public interface MemberFactory {

	public Member make();
	
}
