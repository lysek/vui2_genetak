package ga.salesman;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import ga.Population;
import ga.Reporter;

public class SalesmanReporter extends JPanel implements Reporter {
	
	private Point[] cities;
	private SalesmanMember best;
	private JFrame f;
	
	public SalesmanReporter(Point[] cities) {
		this.cities = cities;
		f = new JFrame("Salesman");
		f.setBounds(100, 100, 450, 450);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.add(this);
	}
	
	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.BLUE);
		
		int[] chromosome = best.getCityOrder();
		Point prev = cities[chromosome[chromosome.length - 1]];
		//Point prev = new Point(0, 0);
		for(int i : chromosome) {
			Point p = cities[i];
			g.drawLine(prev.x * 4, prev.y * 4, p.x * 4, p.y * 4);
			prev = p;
		}
		g.setColor(Color.RED);
		int i = 0;
		for(Point p : cities) {
			g.drawOval((p.x * 4) -2, (p.y * 4) - 2, 4, 4);
			g.drawString(Integer.toString(i), p.x*4, p.y*4);
			i++;
		}
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, 400, 400);
	}

	@Override
	public void report(Population pop, int epoch) {
		best = (SalesmanMember) pop.getBest();
		try {
			SwingUtilities.invokeAndWait(() -> {
			    repaint();
			});
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

}
