package ga.salesman;

import java.util.Random;

import ga.Member;
import ga.operators.Mating;
import javafx.util.Pair;

public class SalesmanMating extends Mating {
	
	static void swapValues(int[] arr, int p1, int p2) {
		int tmp = arr[p1];
		arr[p1] = arr[p2];
		arr[p2] = tmp;
	}
	
	private void swapCities(SalesmanMember m, int city1, int city2) {
		int[] chromosome = m.getCityOrder();
		int p1 = indexOf(chromosome, city1);
		int p2 = indexOf(chromosome, city2);
		swapValues(chromosome, p1, p2);
	}

	private int indexOf(int[] chromosome, int city) {
		for(int i = 0; i < chromosome.length; i++) {
			if(chromosome[i] == city) {
				return i;
			}
		}
		return -1;
	}
	
	/**
	 * prehozeni 2 cilovych mest
	 * 1) najdeme stejne mesto v obou chromozomech
	 * 2) najdeme kam se z neho pokracuje
	 * 3) prohodime
	 * 
	 * A ... -> 21 -> 8 -> 99 -> 17 -> 6 -> ...
	 *                -    X
	 * B ... -> 84 -> 8 -> 6 -> 24 -> 42 -> 12 -> 55 -> 99 -> ...
	 * 
	 * A: 8 -> 6, 17 -> 99
	 * B: 8 -> 99, 55 -> 6
	 * 
	 * tzn. zamenime 6 a 99 v obou chromozomech, pozor na konec chromozomu
	 */
	@Override
	protected Pair<Member, Member> mate(Member parent1, Member parent2) {
		SalesmanMember offspring1 = (SalesmanMember)parent1.cloneMember();
		SalesmanMember offspring2 = (SalesmanMember)parent2.cloneMember();

		Random r = new Random();
		
		int l = offspring1.getCityOrder().length;
		int sl = r.nextInt(l / 2) + 1;
		int randCity = r.nextInt(l);
		
		int p1 = indexOf(offspring1.getCityOrder(), randCity) + 1;
		int p2 = indexOf(offspring2.getCityOrder(), randCity) + 1;
		for(int i = 1; i < sl; i++) {
			if(p1 == l) {
				p1 = 0;
			}
			if(p2 == l) {
				p2 = 0;
			}
			
			int city1 = offspring1.getCityOrder()[p1];
			int city2 = offspring2.getCityOrder()[p2];
			
			swapCities(offspring1, city1, city2);
			swapCities(offspring2, city1, city2);
			p1++;
			p2++;
		}
		return new Pair<Member, Member>(offspring1, offspring2);
	}

}
