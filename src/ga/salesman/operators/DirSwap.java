package ga.salesman.operators;

import ga.GeneticOperator;
import ga.Member;
import ga.salesman.SalesmanMember;

public class DirSwap implements GeneticOperator {
	
	private double prob = 0.05;

	@Override
	public Member[] use(Member[] members) {
		SalesmanMember[] ret = new SalesmanMember[members.length];
		for(int i = 0; i < ret.length; i++) {
			ret[i] = (SalesmanMember) members[i].cloneMember();
			if(Math.random() <= prob) {
				ret[i].reverseDirection();
			}
		}
		return ret;
	}
	
	public void setProbability(double p) {
		prob = p;
	}

}
