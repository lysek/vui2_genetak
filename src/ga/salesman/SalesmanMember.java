package ga.salesman;

import ga.Member;
import java.awt.Point;
import java.util.Random;

/**
 *
 * @author Jiří Lýsek
 */
public class SalesmanMember extends Member {

	private Point[] cities;
	private double fitness;
	private int[] chromosome;

	SalesmanMember(Point[] cities) {
		this.cities = cities;
		this.chromosome = new int[cities.length];
	}

	@Override
	public String toString() {
		String s = "Mesta: ";
		String sep = "";
		for(int i = 0; i < chromosome.length; i++) {
			s += sep + chromosome[i];
			sep = " -> ";
		}
		return s;
	}

	private double calcDistance(Point p1, Point p2) {
		double d1 = p1.x - p2.x;
		double d2 = p1.y - p2.y;
		return Math.sqrt(d1 * d1 + d2 * d2);
	}

	@Override
	public void eval() {
		int len = chromosome.length;
		Point p1, p2;
		p1 = cities[chromosome[0]];
		p2 = cities[chromosome[len - 1]];
		double distance = calcDistance(p1, p2);
		for(int i = 0; i < len - 1; i++) {
			p1 = cities[chromosome[i]];
			p2 = cities[chromosome[i + 1]];
			distance += calcDistance(p1, p2);
		}
		fitness = distance;
	}

	@Override
	public double getFitness() {
		return fitness;
	}

	@Override
	public Member cloneMember() {
		SalesmanMember clone = new SalesmanMember(cities);
		for(int i = 0; i < chromosome.length; i++) {
			clone.chromosome[i] = chromosome[i];
		}
		clone.fitness = fitness;
		return clone;
	}


	@Override
	public void mutate() {
		Random r = new Random();
		int p1, p2;
		do {
			p1 = r.nextInt(chromosome.length);
			p2 = r.nextInt(chromosome.length);
		} while(p1 == p2);
		SalesmanMating.swapValues(chromosome, p1, p2);
	}

	@Override
	public void init() {
		int index;
		Random r = new Random();
		//naplnit sekvencne
		chromosome = new int[cities.length];
		for(int i = 0; i < chromosome.length; i++) {
			chromosome[i] = i;
		}
		//zamichat
		for(int i = chromosome.length - 1; i > 0; i--) {
			index = r.nextInt(i + 1);
			SalesmanMating.swapValues(chromosome, index, i);
		}
	}
	
	public int[] getCityOrder() {
		return chromosome;
	}

	/**
	 * pretoceni casti sekvence
	 * 
	 * ... -> 8 -> 12 -> 54 -> 5 -> 15 -> 19 -> ...
	 *             X                X
	 * ... -> 8 -> 15 -> 5 -> 54 -> 12 -> 19 -> ...
	 * 
	 * muze pomoci pri prevraceni smycek
	 */
	public void reverseDirection() {
		Random r = new Random();
		int ps = r.nextInt(chromosome.length);
		int l = r.nextInt(chromosome.length / 4) + 1;

		//cast chromozomu vykopirovat...
		int p = ps;
		int[] tmp = new int[l];
		for(int i = 0; i < l; i++) {
			if(p >= chromosome.length) {
				p = 0;
			}

			tmp[i] = chromosome[p];

			p++;
		}

		//... a vratit, ale opacne
		p = ps;
		for(int i = 0; i < l; i++) {
			if(p >= chromosome.length) {
				p = 0;
			}

			chromosome[p] = tmp[l - i - 1];

			p++;
		}
	}

}
