package ga.salesman;

import java.awt.Point;
import java.util.Random;

import ga.GeneticAlgorithm;
import ga.GeneticException;
import ga.Population;
import ga.helpers.FitnessLoggerReporter;
import ga.operators.Mutation;
import ga.operators.TournamentSelection;
import ga.salesman.operators.DirSwap;

/**
 * problem obchodniho cestujiciho
 * 
 * @author Jiří Lýsek
 */
public class Salesman {

	private static int popSize = 500;
	private static int epochCount = 2000;
	private static int cityCount = 100;
	private static Point[] cities;
	
	private static Point[] genCities() {
		Random r = new Random();
		Point[] cities = new Point[cityCount];
		for(int i = 0; i < cityCount; i++) {
			Point p = new Point(r.nextInt(100), r.nextInt(100));
			cities[i] = p;
		}
		return cities;
	}

	public static void main(String[] args) throws GeneticException {
		cities = genCities();
		
		Population pop = new Population(popSize);
		pop.addOperator(new TournamentSelection());
		pop.addOperator(new SalesmanMating());
		pop.addOperator(new Mutation());
		pop.addOperator(new DirSwap());

		pop.setMemberFactory(() -> {
			return new SalesmanMember(cities);
		});

		GeneticAlgorithm ga = new GeneticAlgorithm(epochCount);
		ga.setPopulation(pop);
		ga.addReporter(new FitnessLoggerReporter("salesman.csv"));
		ga.addReporter(new SalesmanReporter(cities));
		ga.solve();
	}

}
