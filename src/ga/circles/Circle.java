package ga.circles;

public class Circle {
	
	public double x;
	public double y;
	public double radius;
	
	public Circle(double x, double y, double r) {
		this.x = x;
		this.y = y;
		radius = r;
	}
	
	/**
	 * zjisti, jestli se kruznice prekryva s jinou
	 * 
	 * @param other
	 * @return
	 */
	public boolean overlap(Circle other) {
		double d1 = x - other.x;
		double d2 = y - other.y;
		double dist = Math.sqrt(d1*d1 + d2*d2);
		return dist < radius + other.radius;
	}

}
