package ga.circles;

import ga.Member;
import ga.operators.Mating;
import javafx.util.Pair;

public class CirclesMating extends Mating {

	/**
	 * ruzme moznosti, napr. prevzeti x nebo y souradnice z partnera
	 * radius napr. jako prumer z obou
	 */
	@Override
	protected Pair<Member, Member> mate(Member parent1, Member parent2) {
		CirclesMember offspring1 = (CirclesMember)parent1.cloneMember();
		CirclesMember offspring2 = (CirclesMember)parent2.cloneMember();
		
		double x1, y1, x2, y2;
		if(Math.random() < 0.5) {
			x1 = offspring1.getCircle().x;
			y1 = offspring2.getCircle().y;

			x2 = offspring2.getCircle().x;
			y2 = offspring1.getCircle().y;
		} else {
			x1 = offspring2.getCircle().x;
			y1 = offspring1.getCircle().y;

			x2 = offspring1.getCircle().x;
			y2 = offspring2.getCircle().y;
		}
		
		double r = (offspring1.getCircle().radius + offspring2.getCircle().radius) / 2;
				
		offspring1.circle = new Circle(x1, y1, r);
		offspring2.circle = new Circle(x2, y2, r);
		
		return new Pair<Member, Member>(offspring1, offspring2);
	}

}
