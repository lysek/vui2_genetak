package ga.circles;

import ga.GeneticAlgorithm;
import ga.GeneticException;
import ga.Population;
import ga.StrategySelector;
import ga.helpers.FitnessLoggerReporter;
import ga.operators.Mutation;
import ga.operators.TournamentSelection;

/**
 * ukolem je najit pozici pro co nejvetsi krznici v danem prostoru mezi jinymi kruhy
 * 
 * @author Jiří Lýsek
 *
 */
public class Circles {
	
	private static int popSize = 30;
	private static int epochCount = 200;
	private static Circle[] circles = {new Circle(100,100,30), new Circle(80,10,10), new Circle(30,20,10), new Circle(40,100,20), new Circle(100,50,5), new Circle(0,70,20)};
	private static int max = 100;	//maximalni souradnice

	public static void main(String[] args) throws GeneticException {
		TournamentSelection ts = new TournamentSelection();
		ts.setStrategy(StrategySelector.MAXIMISE);
		
		Population pop = new Population(popSize);
		pop.addOperator(ts);
		pop.addOperator(new CirclesMating());
		pop.addOperator(new Mutation());
		pop.setStrategy(StrategySelector.MAXIMISE);

		pop.setMemberFactory(() -> {
			return new CirclesMember(circles, max);
		});

		GeneticAlgorithm ga = new GeneticAlgorithm(epochCount);
		ga.setPopulation(pop);
		ga.addReporter(new FitnessLoggerReporter("circles.csv"));
		ga.addReporter(new CircleReporter(circles));
		ga.solve();
	}
}
