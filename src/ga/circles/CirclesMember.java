package ga.circles;

import java.util.Random;

import ga.Member;

public class CirclesMember extends Member {
	
	private Circle[] circles;
	Circle circle;
	private double fitness;
	private double max;

	public CirclesMember(Circle[] circles, double max) {
		this.circles = circles;
		this.max = max;
	}

	@Override
	public void eval() {
		for(Circle c : circles) {
			if(c.overlap(circle)) {
				fitness = 0;
				return;
			}
		}
		fitness = Math.PI * circle.radius * circle.radius;
	}

	@Override
	public double getFitness() {
		return fitness;
	}

	@Override
	public Member cloneMember() {
		CirclesMember ret = new CirclesMember(circles, max);
		ret.circle = new Circle(circle.x, circle.y, circle.radius);
		ret.fitness = fitness;
		return ret;
	}

	/**
	 * nahodna zmena pozice kruhu a polomeru
	 */
	@Override
	public void mutate() {
		Random r = new Random();
		circle.x = Math.min(max, circle.x  * (0.5 + r.nextDouble()));
		circle.y = Math.min(max, circle.y  * (0.5 + r.nextDouble()));
		circle.radius *= 0.5 + r.nextDouble();
	}

	@Override
	public void init() {
		Random r = new Random();
		circle = new Circle(r.nextDouble() * max, r.nextDouble() * max, r.nextDouble() * max);		
	}

	@Override
	public String toString() {
		return "[" + circle.x + ", " + circle.y + "] r = " + circle.radius;
	}
	
	public Circle getCircle() {
		return circle;
	}

}
