package ga.circles;

import java.awt.Color;
import java.awt.Graphics;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import ga.Population;
import ga.Reporter;

public class CircleReporter extends JPanel implements Reporter {
	
	private Circle[] circles;
	private CirclesMember best;
	private JFrame f;
	
	public CircleReporter(Circle[] circles) {
		this.circles = circles;
		f = new JFrame("Krouzky");
		f.setBounds(100, 100, 300, 300);
		f.setVisible(true);
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.add(this);
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.BLACK);
		for(Circle c : circles) {
			g.drawOval((int)(c.x - c.radius) * 2, (int)(c.y - c.radius) * 2, (int)(c.radius * 2) * 2, (int)(c.radius * 2) * 2);
		}
		g.drawRect(0, 0, 200, 200);
		if(best != null) {
			g.setColor(Color.RED);
			g.drawOval((int)(best.getCircle().x - best.getCircle().radius) * 2, (int)(best.getCircle().y - best.getCircle().radius) * 2, (int)(best.getCircle().radius * 2) * 2, (int)(best.getCircle().radius * 2) * 2);
		}
	}

	@Override
	public void report(Population pop, int epoch) {
		best = (CirclesMember) pop.getBest();
        
        try {
			SwingUtilities.invokeAndWait(() -> {
			    repaint();
			});
			Thread.sleep(50);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void stop() {
		
	}

}
