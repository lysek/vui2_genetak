package ga;

/**
 * jedinec
 * 
 * @author Jiří Lýsek
 */
abstract public class Member {

	/**
	 * spusti vypoce fitness
	 */
	abstract public void eval();

	/**
	 * getter
	 *
	 * @return
	 */
	abstract public double getFitness();

	/**
	 * vytvori kopii
	 *
	 * @return
	 */
	abstract public Member cloneMember();

	/**
	 * mutace sebe sama
	 */
	abstract public void mutate();

	/**
	 * nahodna inicializace
	 */
	abstract public void init();

	abstract public String toString();

}
