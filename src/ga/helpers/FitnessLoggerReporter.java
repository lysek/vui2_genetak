package ga.helpers;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import ga.Member;
import ga.Population;
import ga.Reporter;

public class FitnessLoggerReporter implements Reporter {
	
	private PrintWriter printWriter;
	private FileWriter writer;

	public FitnessLoggerReporter(String file) {
		try {
			writer = new FileWriter(file);
			printWriter = new PrintWriter(writer);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void report(Population pop, int epoch) {
		System.out.println("Epocha " + epoch);
		Member best = pop.getBest();
		System.out.println(best);
		System.out.println("Fitness: " + best.getFitness());
		printWriter.println(best.getFitness());
	}
	
	@Override
	public void stop() {
		try {
			printWriter.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}