package ga;

/**
 * operace pobiha nad celou populaci a vraci novou populaci jedincu
 * 
 * @author Jiří Lýsek
 */
public interface GeneticOperator {
	
	public Member[] use(Member[] members);

}
