package ga.operators;

import java.util.Random;

import ga.GeneticOperator;
import ga.Member;
import ga.StrategySelector;

/**
 * operator turnajove selekce
 * bere n-tice jedincu a vybira pro dalsi operator nejlepsiho
 * 
 * @author Jiří Lýsek
 */
public class TournamentSelection implements GeneticOperator, StrategySelector {

	private int size = 4;
	private int strategy = StrategySelector.MINIMISE;
	
	public void setStrategy(int s) {
		strategy = s;
	}

	private Member tournament(Member[] members) {
		Member ret = null;
		Random r = new Random();
		
		int p = r.nextInt(members.length);
		ret = members[p];
		
		for(int i = 1; i < size; i++) {
			p = r.nextInt(members.length);
			if(strategy == StrategySelector.MINIMISE && ret.getFitness() > members[p].getFitness()) {
				ret = members[p];
			} else if(strategy == StrategySelector.MAXIMISE && ret.getFitness() < members[p].getFitness()) {
				ret = members[p];
			}
		}
		return ret;
	}

	@Override
	public Member[] use(Member[] members) {
		Member[] ret = new Member[members.length];
		for(int i = 0; i < members.length; i++) {
			Member winner = tournament(members);
			ret[i] = winner.cloneMember();
		}
		return ret;
	}

}
