package ga.operators;

import ga.GeneticOperator;
import ga.Member;
import javafx.util.Pair;

/**
 * operator krizeni
 * bere pary po sobe jdoucich jedincu z populace a vytvari nove jedince
 *
 * @author Jiří Lýsek
 */
abstract public class Mating implements GeneticOperator {

	private double prob = 0.8;

	@Override
	public Member[] use(Member[] members) {
		Member[] ret = new Member[members.length];
		for(int i = 0; i < members.length; i+= 2) {
			if(Math.random() <= prob) {
				//krizit
				Pair<Member, Member> tmp = mate(members[i], members[i + 1]);
				ret[i] = tmp.getKey();
				ret[i + 1] = tmp.getValue();
			} else {
				//kopirovat rodice
				ret[i] = members[i].cloneMember();
				ret[i + 1] = members[i + 1].cloneMember();
			}
		}
		return ret;
	}
	
	public void setProbability(double p) {
		prob = p;
	}
	
	protected abstract Pair<Member, Member> mate(Member parent1, Member parent2);

}
