package ga.operators;

import ga.GeneticOperator;
import ga.Member;

/**
 * operator mutace
 * 
 * @author Jiří Lýsek
 */
public class Mutation implements GeneticOperator {

	private double prob = 0.05;

	@Override
	public Member[] use(Member[] members) {
		Member[] ret = new Member[members.length];
		for(int i = 0; i < members.length; i++) {
			ret[i] = members[i].cloneMember();
			if(Math.random() <= prob) {
				ret[i].mutate();
			}
		}
		return ret;
	}
	
	public void setProbability(double p) {
		prob = p;
	}

}
