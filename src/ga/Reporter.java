package ga;

/**
 * hlaseni vysledku epochy
 * 
 * @author Jiří Lýsek
 *
 */
public interface Reporter {
	
	/**
	 * report populace
	 * 
	 * @param pop
	 * @param epoch
	 */
	public void report(Population pop, int epoch);
	
	/**
	 * ukonceni procesu
	 */
	public void stop();

}
