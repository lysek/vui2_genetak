package ga;

public class GeneticException extends Exception {

	public GeneticException(String s) {
		super(s);
	}

}
