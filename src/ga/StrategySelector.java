package ga;

public interface StrategySelector {
	
	public static final int MAXIMISE = 1;
	public static final int MINIMISE = -1;
	
	public void setStrategy(int s);

}
