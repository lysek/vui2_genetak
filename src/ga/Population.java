package ga;

import java.util.ArrayList;
import java.util.List;

/**
 * populace
 * 
 * @author Jiří Lýsek
 */
public class Population implements StrategySelector {

	private int size;
	private MemberFactory mf;
	private Member[] members;
	private List<GeneticOperator> operators = new ArrayList<GeneticOperator>();
	private int strategy = StrategySelector.MINIMISE;
	
	public void setStrategy(int s) {
		strategy = s;
	}
	
	public Population(int size) {
		this.size = size;
	}

	public void setMemberFactory(MemberFactory mf) {
		this.mf = mf;
	}

	/**
	 * inicializace novych jedincu
	 * @throws GeneticException 
	 */
	public void init() throws GeneticException {
		if(mf == null) {
			throw new GeneticException("Set MemberFactory first.");
		}
		members = new Member[size];
		for(int i = 0; i < size; i++) {
			members[i] = mf.make();
			members[i].init();
		}
	}

	/**
	 * spusti operatory
	 * @throws GeneticException 
	 */
	void operators() throws GeneticException {
		if(operators.size() == 0) {
			throw new GeneticException("Add some GeneticOperators first.");
		}
		for(GeneticOperator op : operators) {
			members = op.use(members);
		}
	}

	/**
	 * spusteni vypoctu fitness
	 */
	void eval() {
		for(int i = 0; i < size; i++) {
			members[i].eval();
		}
	}

	public void addOperator(GeneticOperator op) {
		operators.add(op);
	}

	/**
	 * vraci nejlepsiho jedince
	 * 
	 * @return
	 */
	public Member getBest() {
		Member best = members[0];
		for(int i = 1; i < members.length; i++) {
			if(strategy == StrategySelector.MINIMISE && best.getFitness() > members[i].getFitness()) {
				best = members[i];
			} else if(strategy == StrategySelector.MAXIMISE && best.getFitness() < members[i].getFitness()) {
				best = members[i];
			}
		}
		return best;
	}

	public Member[] getMembers() {
		return members;
	}

}
