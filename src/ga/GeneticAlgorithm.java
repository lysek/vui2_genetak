/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ga;

import java.util.ArrayList;
import java.util.List;

/**
 * kostra genetickeho algoritmu
 * 
 * @author Jiří Lýsek
 */
public class GeneticAlgorithm {

	private int epochCount;

	private Population pop;
	
	private List<Reporter> reporters = new ArrayList<Reporter>();

	public GeneticAlgorithm(int epochCount) {
		this.epochCount = epochCount;
	}

	public void setPopulation(Population p) throws GeneticException {
		pop = p;
		pop.init();
		pop.eval();
	}

	public void solve() throws GeneticException {
		for(int epoch = 0; epoch < epochCount; epoch++) {
			pop.operators();
			pop.eval();
			final int fepoch = epoch;
			reporters.forEach((r) -> {
				r.report(pop, fepoch);
			});
		}
		
		reporters.forEach((r) -> {
			r.stop();
		});
	}

	public void addReporter(Reporter reporter) {
		reporters.add(reporter);
	}

}
