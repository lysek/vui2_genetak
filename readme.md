Jednoduchy priklad vyuziti geneticky algoritmu
==============================================

Implementuje 2 ulohy:
- Obchodni cestujici
- Hledani umisteni co nejvetsiho kruhu v danem 2D prostoru mezi jinymi kruhy

Spousti se pres src/ga/salesman/Salesman.java nebo src/ga/circles/Circles.java.

Nevyzaduje zadne specialni zavislosti.